package com.tinygray.multicontroller.controller;

import com.tinygray.multicore.vo.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("user")
public class UserController {
    private static final Logger log = LoggerFactory.getLogger(UserController.class);
    @GetMapping("getUserInfo")
    public User getUserInfo() {
        return new User("tinygray", "女", 18);
    }
}
