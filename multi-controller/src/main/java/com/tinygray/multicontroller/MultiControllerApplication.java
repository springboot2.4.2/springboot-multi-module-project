package com.tinygray.multicontroller;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultiControllerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MultiControllerApplication.class, args);
    }

}
